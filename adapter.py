
echo `$@| ./listener.sh |./calculator.sh` 
``def generateDiagram(input_folder,input_file,tag_folder, output_folder):
   data=pd.read_csv(input_folder+input_file,sep='\t')
   project_name=input_file.split('.')[0]
   fig, ax1 = pyplot.subplots()
   ax2=ax1.twinx()
   ax1.plot(data.ordinal, data.nb_smells, 'g-')
   ax2.plot(data.ordinal, data.nb_classes, 'b-')
   ax1.set_xlabel('Commits')
   ax1.set_ylabel('number of smells', color='g')
   ax2.set_ylabel('number of classes', color='b')
#     if not data.ordinal.empty:
#         pace=int(max(data.ordinal)/20)
#         if pace!=0 :
#             arr = np.arange(min(data.ordinal), max(data.ordinal)+1, pace)
#             pyplot.xticks(arr)
   fig.set_size_inches(18.5, 10.5)
   tag_data=pd.read_csv(tag_folder+input_file,sep='\t')
   tag_ordinals=tag_data.ordinal
   for tag in tag_ordinals:
       pyplot.axvspan(tag, tag, color='red', alpha=0.5)
   fig_name=output_folder+ project_name+'.png'
   fig.savefig(fig_name, dpi=100)
   pyplot.close()''