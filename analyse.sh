#!/bin/bash
function=$1
 # replaceing : with _ for othertags
echo $name


benchmark="tommtiv1"

machinename=$(cat 'machinename')
dirname=$(cat 'dirname')
mkdir -p $dirname/$name/$instance
cd $dirname/$name/$instance

programslist=(chakibmed/icse_pyston:$benchmark
chakibmed/icse_pypy3:$benchmark
chakibmed/icse_pypy2:$benchmark
chakibmed/icse_numba3:$benchmark
chakibmed/icse_numba2:$benchmark
chakibmed/icse_nuitka:$benchmark
chakibmed/icse_cython:$benchmark
chakibmed/icse_cpython3:$benchmark
chakibmed/icse_cpython2:$benchmark
chakibmed/icse_activepython:$benchmark)

for a in ${programslist[*]}; # the test 
do 
    # k=$(( 2 ** i ))
    # for j in {0..20} # we have to add severals iterations inorder to get a more stable measures 
    # do
        name=${a#*/} # removing the ownername // general chakibmed
        name=${name/:latest/} # remove the tag if its latest
        name=${name/:/_}
        name1=$name"_"$function
        echo $name $function 
        docker run --rm -it  --name  $name1 --cpuset-cpus 0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38  $a $function | ./../../listener.sh >> $name1.txt

        #cpus
        head -n 1 /tmp/powerapi-reporting/*-cpu-0-powerrep.csv > power_cpu_0_$name1.csv
        head -n 1 /tmp/powerapi-reporting/*-cpu-1-powerrep.csv > power_cpu_1_$name1.csv
        #dram
        head -n 1 /tmp/powerapi-reporting/*-dram-0-powerrep.csv > power_dram_0_$name1.csv
        head -n 1 /tmp/powerapi-reporting/*-dram-0-powerrep.csv > power_dram_1_$name1.csv
        #cpus
        egrep $name1 /tmp/powerapi-reporting/*-cpu-0-powerrep.csv >> power_cpu_0_$name1.csv
        egrep $name1 /tmp/powerapi-reporting/*-cpu-1-powerrep.csv >> power_cpu_1_$name1.cs
        #dram
        egrep $name1 /tmp/powerapi-reporting/*-dram-0-powerrep.csv >> power_dram_0_$name1.csv
        egrep $name1 /tmp/powerapi-reporting/*-dram-0-powerrep.csv >> power_dram_1_$name1.csv
        sleep 5 
    # done
    
done 

chown -R mbelgaid .  
su mbelgaid -c "mkdir -p  /home/mbelgaid/$dirname/$name.$instance" # direname is date/machinename/programename/instance - this is the case of hillclimbing , but later we may change it 
su mbelgaid -c "cp -R * /home/mbelgaid/$dirname/$name.$instance"
